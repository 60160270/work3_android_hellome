package buu.supawee.hellome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val LOG = "MAIN_ACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val edtName = findViewById(R.id.edtName) as EditText
        val btnHello = findViewById<Button>(R.id.btnHello)
        val txtHello = findViewById<TextView>(R.id.txtHello)
        // btnHello.setOnClickListener(this)
//        btnHello.setOnClickListener(object:View.OnClickListener{
//            override fun onClick(view: View?) {
//                Toast.makeText(this@MainActivity,"Click Me",Toast.LENGTH_LONG).show()
//            }
//        })
        btnHello.setOnClickListener{
            Toast.makeText(this@MainActivity,"Hello ${edtName.text.toString()}",Toast.LENGTH_LONG).show()
            txtHello.text = "Hello ${edtName.text.toString()}"
            Log.d(LOG,"Click Me")
        }
    }

    override fun onClick(view: View?) {
        Toast.makeText(this,"Click Me",Toast.LENGTH_LONG).show()
    }
}